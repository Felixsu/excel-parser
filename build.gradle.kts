plugins {
    kotlin("jvm") version "1.3.61"
    application
}

group = "com.lixionary"
version = "1.0.0"

application {
    mainClassName = "com.lixionary.exp.MainKt"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.apache.commons:commons-lang3:3.0")
    implementation("org.apache.commons:commons-text:1.6")
    implementation("commons-codec:commons-codec:1.11")
    implementation("org.apache.poi:poi:3.15")
    implementation("org.apache.poi:poi-ooxml:3.15")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}
