package com.lixionary.exp

import com.lixionary.exp.bean.DogBean
import com.lixionary.exp.service.DogExcelService
import com.lixionary.exp.service.ExcelService
import java.io.File

fun main(args: Array<String>) {
    val fileName = "Java Example.xlsx"
    val classLoader = object {}.javaClass.classLoader
    val fileURL = classLoader.getResource(fileName)

    val excelService: ExcelService<DogBean> = DogExcelService()
    val file = File(checkNotNull(fileURL?.toURI()))
    val results: List<DogBean> = excelService.read(file, true)

    results.forEach{ println(it) }
}
