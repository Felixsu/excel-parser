package com.lixionary.exp.bean

import java.time.LocalDate

class DogBean(
    val no: Int,
    val name: String,
    val breed: String,
    val age: Double,
    val gender: String,
    val startDate: LocalDate) {

    override fun toString(): String {
        return "DogBean(no=$no, name='$name', breed='$breed', age=$age, gender='$gender', startDate=$startDate)"
    }
}
