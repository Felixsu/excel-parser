package com.lixionary.exp.service

import com.lixionary.exp.bean.DogBean
import org.apache.poi.ss.usermodel.Row
import java.time.ZoneId
import java.util.*

class DogExcelService: ExcelService<DogBean>() {

    override fun construct(row: Row): DogBean? {
        return try {
            val no: Int = checkNotNull(row.getCell(0)?.numericCellValue?.toInt())
            val name: String = checkNotNull(row.getCell(1)?.stringCellValue)
            val breed: String = checkNotNull(row.getCell(2)?.stringCellValue)
            val age: Double = checkNotNull(row.getCell(3)?.numericCellValue)
            val gender: String = checkNotNull(row.getCell(4)?.stringCellValue)
            val startDate: Date = checkNotNull(row.getCell(5)?.dateCellValue)
            DogBean(no, name, breed, age, gender, startDate.toInstant().atZone(ZoneId.of("UTC")).toLocalDate())
        } catch (ex: IllegalStateException) {
            null
        }
    }
}
