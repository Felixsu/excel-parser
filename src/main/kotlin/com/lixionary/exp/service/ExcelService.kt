package com.lixionary.exp.service

import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File

abstract class ExcelService<T> {

    abstract fun construct(row: Row): T?

    fun read(file: File, useHeader: Boolean): List<T> {
        val results: MutableList<T> = ArrayList()
        val workbook = XSSFWorkbook(file)
        val sheet: Sheet = workbook.getSheetAt(0)
        val nRow = sheet.lastRowNum

        var rowIdx: Int = if (useHeader) 1 else 0
        do {
            val bean: T? = sheet.getRow(rowIdx)?.let { construct(it) }
            bean?.let { results.add(it) }
            rowIdx++
        } while (rowIdx <= nRow)
        return results
    }
}
